/**
 * 
 */
package co.th.gosoft.etaxinbound.demo.exception;

/**
 * @author thepphithakpat
 *
 */
public class ValidationException extends RuntimeException {
	private static final long serialVersionUID = -2660131243853312822L;

	public ValidationException(String message) {
		super(message);
	}  

}
