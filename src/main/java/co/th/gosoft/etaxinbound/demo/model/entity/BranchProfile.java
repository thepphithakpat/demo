/**
 * 
 */
package co.th.gosoft.etaxinbound.demo.model.entity;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.IdClass;
import javax.persistence.Table;

import co.th.gosoft.etaxinbound.demo.model.pk.BranchProfileId;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

/**
 * @author thepphithakpat
 *
 */
@Entity
@IdClass(BranchProfileId.class)
@Table(name = "BRANCH_PROFILE")
@NoArgsConstructor
@Setter
@Getter
public class BranchProfile  {
	@Id
	@Column(name = "BRANCH_CODE", nullable = false, length = 5)
	private String branchCode;
	
	@Id
	@Column(name = "TRANS_DATE", nullable = false)
	private Date transDate;
	
	@Column(name = "AREA_CODE", nullable = true, length = 2)
	private String areaCode;
	
	@Column(name = "BRANCH_TYPE", nullable = true, length = 10)
	private String branchType;
	
	@Column(name = "BRANCH_BUSINESS_TYPE", nullable = true, length = 2)
	private String branchBusinessType;
	
	@Column(name = "IS_MERGE_SEVEN_ELEVEN", nullable = true)
	private char isMergeSevenEleven;
	
	@Column(name = "IS_MERGE_BOOKSMILE", nullable = true)
	private char isMergeBookSmile;
	
	@Column(name = "IS_MERGE_KUDSAN", nullable = true)
	private char isMergeKudsan;
	
	@Column(name = "IS_MERGE_YUURI", nullable = true)
	private char isMergeYuuri;
	
	@Column(name = "IS_MERGE_VENDING_MACHINE", nullable = true)
	private char isMergeVendingMachine;
	
	@Column(name = "IS_FRANCHISE", nullable = true)
	private char IsFranchise;
	
	@Column(name = "IS_SUBAREA", nullable = true)
	private char isSubArea;
	
	@Column(name = "BRANCH_NAME", nullable = true)
	private String branchName;
	
	@Column(name = "IS_MERGE_EXTA", nullable = true)
	private char isMergeExta;
	
	@Column(name = "PROVINCE_CODE", nullable = true)
	private String provinceCode;
	
	@Column(name = "AREA_NAME", nullable = true)
	private String areaName;
	
	@Column(name = "ZONE_ID", nullable = true)
	private String zoneId;
}
