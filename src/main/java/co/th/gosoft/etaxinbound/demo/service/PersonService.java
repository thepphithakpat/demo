package co.th.gosoft.etaxinbound.demo.service;

import java.util.List;

import co.th.gosoft.etaxinbound.demo.controller.request.PersonRequest;
import co.th.gosoft.etaxinbound.demo.model.entity.Person;

 

public interface PersonService {
	List<Person> getAllPerson();
	Person getPersonById(Long id);
	Person createPerson(PersonRequest personRequest);
	List<Person> searchPersonClassic();
}
