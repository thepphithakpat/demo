/**
 * 
 */
package co.th.gosoft.etaxinbound.demo.controller.api;

import java.util.List;

import javax.validation.Valid;

import org.springframework.http.HttpStatus;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;

import co.th.gosoft.etaxinbound.demo.controller.request.PersonRequest;
import co.th.gosoft.etaxinbound.demo.exception.ValidationException;
import co.th.gosoft.etaxinbound.demo.model.entity.Person;
import co.th.gosoft.etaxinbound.demo.service.PersonService; 

/**
 * @author thepphithakpat
 *
 */

@RestController
@RequestMapping("/person")
public class PersonController {
	private final PersonService personService;

	public PersonController(PersonService personService) {
		this.personService = personService;
	}

	@GetMapping()
	public List<Person> getPersons() {
		return personService.getAllPerson();
	}
	
	@GetMapping("/{id}")
	public Person getPerson(@PathVariable long id) {
		return personService.getPersonById(id);
	}
	
	@GetMapping("/search")
	public List<Person> searchPersons() {
		return personService.searchPersonClassic();
	}
	
	@ResponseStatus(HttpStatus.CREATED)
	@PostMapping()
	public Person addPerson(@RequestBody @Valid PersonRequest personRequest, BindingResult bindingResult) {
		if (bindingResult.hasErrors()) {
			bindingResult.getFieldErrors().stream().forEach(fieldError -> {
				throw new ValidationException(fieldError.getDefaultMessage() + " : " + fieldError.getField());
			});
		}
		
		return personService.createPerson(personRequest);
	}
	
	@PostMapping("/form-data")
	public PersonRequest testFormDatat(PersonRequest personRequest) {
		return personRequest;
	}
	
	
}