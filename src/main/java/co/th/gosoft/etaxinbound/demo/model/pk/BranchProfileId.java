/**
 * 
 */
package co.th.gosoft.etaxinbound.demo.model.pk;

import java.io.Serializable;
import java.util.Date;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.ToString;

/**
 * @author thepphithakpat
 *
 */

@Setter
@Getter
@AllArgsConstructor 
@NoArgsConstructor
@ToString
public class BranchProfileId implements Serializable { 
	private static final long serialVersionUID = -126279921681031343L; 
	 
	private String branchCode;
	private Date transDate;

}
