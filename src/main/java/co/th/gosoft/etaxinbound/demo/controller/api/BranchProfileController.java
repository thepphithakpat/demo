package co.th.gosoft.etaxinbound.demo.controller.api;

import java.util.List;

import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import co.th.gosoft.etaxinbound.demo.model.entity.BranchProfile;
import co.th.gosoft.etaxinbound.demo.service.BranchProfileService;
import lombok.extern.slf4j.Slf4j;

/**
 * @author thepphithakpat
 *
 */

@RestController
@RequestMapping("/branch-profile")
@Slf4j
public class BranchProfileController {

	private BranchProfileService branchProfileService;

	public BranchProfileController(BranchProfileService branchProfileService) {
		this.branchProfileService = branchProfileService;
	}

	@GetMapping("")
	public String say() {
		return "SPRING HERE";
	}
	
	@GetMapping("/{branchCode}")
	public List<BranchProfile> getBranchById(@PathVariable String branchCode) {
		log.warn("BRANCH CODE " +branchCode);
		return branchProfileService.getBranchProfileByBranchCodeAndTransDate(branchCode);
	}
}
