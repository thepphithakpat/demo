package co.th.gosoft.etaxinbound.demo.service;

import java.util.List;
import java.util.Optional;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.TypedQuery;

import org.springframework.data.domain.Sort;
import org.springframework.stereotype.Service;

import co.th.gosoft.etaxinbound.demo.controller.request.PersonRequest;
import co.th.gosoft.etaxinbound.demo.exception.PersonNotFoundException;
import co.th.gosoft.etaxinbound.demo.model.entity.Person;
import co.th.gosoft.etaxinbound.demo.repository.PersonRepository;

@Service
public class PersonServiceImpl implements PersonService{
	
	private PersonRepository personRepository;
	@PersistenceContext
	private EntityManager em;
	
	public PersonServiceImpl(PersonRepository personRepository) {
		this.personRepository = personRepository;
	}

	@Override
	public List<Person> getAllPerson() { 
		return personRepository.findAll(Sort.by(Sort.Direction.ASC, "id"));
	}

	@Override
	public Person createPerson(PersonRequest personRequest) {
		Person person = new Person();
		person.setName(personRequest.getName())
		.setLastName(personRequest.getLastName())
		.setNickName(personRequest.getNickName())
		.setAge(personRequest.getAge());
		return personRepository.save(person);
	}

	@Override
	public Person getPersonById(Long id) {
		Optional<Person> person = personRepository.findById(id);
		if (person.isPresent()) {
			return person.get();
		}
		throw new PersonNotFoundException(id);
	}

	@SuppressWarnings({ "rawtypes", "unchecked" })
	@Override
	public List<Person> searchPersonClassic() {
		TypedQuery query =  (TypedQuery) em.createNativeQuery("SELECT * FROM PERSON ", Person.class);
		return query.getResultList();
	}
	

}
