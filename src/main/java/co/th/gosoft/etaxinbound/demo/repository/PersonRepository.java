/**
 * 
 */
package co.th.gosoft.etaxinbound.demo.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import co.th.gosoft.etaxinbound.demo.model.entity.Person; 

/**
 * @author thepphithakpat
 *
 */
public interface PersonRepository extends JpaRepository<Person, Long> {
	 
}
