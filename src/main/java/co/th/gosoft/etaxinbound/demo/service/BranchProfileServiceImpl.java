/**
 * 
 */
package co.th.gosoft.etaxinbound.demo.service;

import java.sql.Timestamp;
import java.util.Date;
import java.util.List;

import org.springframework.stereotype.Service;

import co.th.gosoft.etaxinbound.demo.model.entity.BranchProfile;
import co.th.gosoft.etaxinbound.demo.repository.BranchProfileRepository;

/**
 * @author thepphithakpat
 *
 */

@Service
public class BranchProfileServiceImpl implements BranchProfileService {
	
	private BranchProfileRepository branchProfileRepository;

	public BranchProfileServiceImpl(BranchProfileRepository branchProfileRepository) {
		this.branchProfileRepository = branchProfileRepository; 
	}

	@Override
	public List<BranchProfile> getBranchProfileByBranchCodeAndTransDate(String branchCode) {
		return branchProfileRepository.findByBranchCodeAndTransDateGreaterThanEqual(branchCode, new Timestamp(new Date(122, 4, 1).getTime()));
	}  
	
}
