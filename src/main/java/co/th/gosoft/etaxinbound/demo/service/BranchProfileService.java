/**
 * 
 */
package co.th.gosoft.etaxinbound.demo.service;

import java.util.List;

import co.th.gosoft.etaxinbound.demo.model.entity.BranchProfile;

/**
 * @author thepphithakpat
 *
 */
public interface BranchProfileService {
	/**
	 *
	 * @param branchCode
	 * @return
	 */
	public List<BranchProfile> getBranchProfileByBranchCodeAndTransDate(String branchCode);
}
