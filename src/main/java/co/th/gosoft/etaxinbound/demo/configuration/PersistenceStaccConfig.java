/**
 * 
 */
package co.th.gosoft.etaxinbound.demo.configuration;

import java.util.Properties;

import javax.sql.DataSource;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Primary;
import org.springframework.data.jpa.repository.config.EnableJpaRepositories;
import org.springframework.orm.jpa.JpaTransactionManager;
import org.springframework.orm.jpa.LocalContainerEntityManagerFactoryBean;
import org.springframework.orm.jpa.vendor.HibernateJpaVendorAdapter;

/**
 * @author thepphithakpat
 *
 */

@Configuration
@EnableJpaRepositories( 
  basePackages = "co.th.gosoft.etaxinbound.demo.repository", 
  entityManagerFactoryRef = "entityManagerFactoryBean", 
  transactionManagerRef = "staccTransactionManager"
) 
public class PersistenceStaccConfig {
	@Autowired
	@Qualifier("staccDatasource")
	private DataSource staccDatasource;

	@Bean
    @Primary
	public LocalContainerEntityManagerFactoryBean entityManagerFactoryBean() {
		LocalContainerEntityManagerFactoryBean em = new LocalContainerEntityManagerFactoryBean();
		em.setDataSource(staccDatasource);
		em.setJpaProperties(properties());
		em.setPackagesToScan(new String[] { "co.th.gosoft.etaxinbound.demo.model.entity" });
		em.setJpaVendorAdapter(new HibernateJpaVendorAdapter());
		return em;
	}

	@Bean
	@Primary
	public JpaTransactionManager staccTransactionManager() {
		final JpaTransactionManager transactionManager = new JpaTransactionManager();
		transactionManager.setEntityManagerFactory(entityManagerFactoryBean().getObject());
		return transactionManager;
	}
	
	private Properties properties() {
	    Properties properties = new Properties();
	    properties.put("hibernate.show_sql", "true");
	    properties.put("hibernate.format_sql", "true");
	    properties.put("hibernate.hbm2ddl.auto", "update");
	    return properties;
	}
}
