/**
 * 
 */
package co.th.gosoft.etaxinbound.demo.controller.request;

import javax.validation.constraints.NotEmpty;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

/**
 * @author thepphithakpat
 *
 */
@Getter
@Setter
@NoArgsConstructor
public class PersonRequest {
	
	@NotEmpty(message = "It Can't Empty !")
	private String name;
	private String lastName;
	private String nickName;
	private int age;
}
