/**
 * 
 */
package co.th.gosoft.etaxinbound.demo.exception;

/**
 * @author thepphithakpat
 *
 */
public class PersonNotFoundException extends RuntimeException { 
	private static final long serialVersionUID = -7950754238273292053L;

	public PersonNotFoundException(long id) {
		super("Not found person id : " +id);
	}
	
}
