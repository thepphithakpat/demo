/**
 * 
 */
package co.th.gosoft.etaxinbound.demo.configuration;

import org.springframework.boot.autoconfigure.jdbc.DataSourceProperties;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Primary;

import oracle.jdbc.pool.OracleDataSource;

/**
 * @author thepphithakpat
 *
 */

@Configuration
public class DataSourceConfig {
	@Bean
	@ConfigurationProperties(prefix = "spring.datasource")
	public DataSourceProperties staccDataSourceProperties(){
		return new DataSourceProperties();
	}

	@Bean("staccDatasource")
	@Primary
	public OracleDataSource staccDataSource(){
		return staccDataSourceProperties().initializeDataSourceBuilder().type(OracleDataSource.class).build();
	}
}
