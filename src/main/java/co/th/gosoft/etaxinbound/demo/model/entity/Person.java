package co.th.gosoft.etaxinbound.demo.model.entity;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.experimental.Accessors;

@Getter
@Setter
@NoArgsConstructor
@Accessors(chain = true)
@Entity
public class Person {
	
	private @Id @GeneratedValue(strategy = GenerationType.AUTO) Long id;
	
	@Column(length = 150, nullable = false, unique = false)
	private String name;
	
	@Column(name = "LAST_NAME", length = 150, nullable = false)
	private String lastName;
	
	@Column(name = "NICK_NAME", length = 50, nullable = false)
	private String nickName;
	private int age;

}
