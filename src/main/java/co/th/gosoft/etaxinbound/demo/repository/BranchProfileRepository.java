/**
 * 
 */
package co.th.gosoft.etaxinbound.demo.repository;

import java.sql.Date;
import java.sql.Timestamp;
import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import co.th.gosoft.etaxinbound.demo.model.entity.BranchProfile;
import co.th.gosoft.etaxinbound.demo.model.pk.BranchProfileId;

/**
 * @author thepphithakpat
 *
 */

public interface BranchProfileRepository extends JpaRepository<BranchProfile, BranchProfileId> {
	
	public List<BranchProfile> findByBranchCodeAndTransDateGreaterThanEqual(String branchCode, Timestamp transDate);
	
//	@Query(value = "SELECT * FROM BRANCH_PROFILE bp WHERE BRANCH_CODE = :BRANCH AND TRANS_DATE = TO_DATE('010522', 'ddMMyy')", nativeQuery = true)
//	public List<BranchProfile> findByBranchCodeAndTransDateGreaterThanEqualNative(@Param("BRANCH") String branchCode, @Param("DATE") Date transDate);
	
//	@Query(value = "SELECT * FROM BRANCH_PROFILE bp WHERE BRANCH_CODE = :BRANCH AND TRANS_DATE = TO_DATE('010522', 'ddMMyy')", nativeQuery = true)
//	public List<BranchProfile> findByBranchCodeAndTransDateGreaterThanEqualNative(@Param("BRANCH") String branchCode);
}
